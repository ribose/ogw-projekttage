<?php
$db_host = 'localhost';
$db_user = 'root';
$db_pwd = '';

$database = 'ogw';

// LDAP
$uid = 'p.rib';
$givenname = 'Philomène';
$surname = 'Ribeaud';
$level = parseLevel('schule-7c');

function parseLevel($level) {
	return intval(preg_replace('/\D/', '', $level));
}

// DB connection
$con = mysqli_connect($db_host, $db_user, $db_pwd, $database);
if (mysqli_connect_errno()) {
	die("Failed to connect to MySQL: " . mysqli_connect_error());
}

// Constants
define("PROJECT_NUMBER", 3);
define("TITLE", "Projekttage " . date("Y"));

// Submitting the form
if ($_POST) {
	$prioritized_list = isset($_POST['course']) ? implode(',', $_POST['course']) : '';
	$student = mysqli_query($con, "select id from students where uid = '{$_POST['uid']}'");
	// Alternative if it does NOT work: 'mysqli_num_rows($result) == 0'
	if ($student) {
		// The student already exists
		$sql = "update students set prioritized_list = '{$prioritized_list}' where uid = '{$_POST['uid']}'";
	}  else {
		// The student does NOT exist yet
		$sql = "insert into students (uid, givenname, lastname, prioritized_list) values ('{$_POST['uid']}', '{$_POST['givenname']}', '{$_POST['lastname']}', '{$prioritized_list}')";
	}

	if (!mysqli_query($con, $sql)) {
    	die("Form submitting failed!");
	}
	$titles = [];
	if (strlen($prioritized_list) > 0) {
		$query = mysqli_query($con, "select title from courses where id in ({$prioritized_list})");
		while ($title = mysqli_fetch_assoc($query)['title']) {
			array_push($titles, $title);
		}
	}
}

// Fetch the courses
$courses = mysqli_query($con, "select id, title, description from courses where level_from <= $level and level_to >= $level order by title");
// Fetch current 'prioritized_list'
$prioritized_list = mysqli_query($con, "select prioritized_list from students where uid = '$uid' limit 1");
$sel_courses = [];
if ($prioritized_list) {
	$val = mysqli_fetch_array($prioritized_list)['prioritized_list'];
	$sel_courses = strlen(trim($val)) > 0 ? explode(',', $val) : [];
}

if (!$courses) {
    die("Query to display courses failed!");
}
?>
<!doctype html>
<html lang="en">
	<head>
		<title><?php echo TITLE; ?></title>
		 <!-- Required meta tags -->
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="bootstrap.min.css" type="text/css" />
	</head>
	<body>
		<script src="jquery-3.3.1.min.js"></script>
		<script src="bootstrap.min.js"></script>
		<script type="text/javascript">
			// When your page is ready, wire up these events
    		$(document).ready(function () {
    			$('#form input[name="course[]"]').on('click', function() {
            		$('#submit').prop('disabled', hasNotChanged() || notExactChecked(<?php echo PROJECT_NUMBER; ?>));
    			});
    		});

    		function notExactChecked(exact) {
    			return $('#form input[name="course[]"]:checked').length !== exact;
    		}

    		function hasNotChanged() {
    			var val = $.trim('<?php echo implode(',', $sel_courses); ?>');
    			var sel_courses = val.length > 0 ? val.split(',').sort() : [];
    			var checked = $('#form input[name="course[]"]:checked').map(function(){
      				return $(this).val();
    			}).toArray().sort();
    			if (checked.length !== sel_courses.length) {
    				console.log("Lengths differ '[" + checked.join(',') + "]' !== '[" + sel_courses.join(',') + "]' (" + checked.length + " !== " + sel_courses.length + "). Returning false...");
    				return false;
    			}
    			return $(sel_courses).not(checked).length === 0 && $(checked).not(sel_courses).length === 0;
    		}
		</script>
		<div class="container">
		<h2><?php echo TITLE; ?></h2>
		<?php if ($_POST) {  ?>
		<div class="alert alert-success" role="alert">
			<?php if (count($titles)) {  ?>
			Du hast dich für folgende Projekte angemeldet:
			<ul>
				<?php 
				foreach ($titles as $title) {
					echo "<li>{$title}</li>";					
				}
				?>
			</ul>
			<?php } else {  ?>
			Du hast dich für kein Projekt angemeldet.
			<?php } ?>			
		</div>
		<?php } ?>
		<div class="alert alert-primary" role="alert">
			Willkommen <b><?php echo $givenname . " " . $surname; ?></b> zur der Anmeldungsseite der Projekttage.<br>
			Bitte wähle <?php echo PROJECT_NUMBER; ?> Projekte aus der unteren liste.
		</div>
		<form method="post" action="#" id="form">
			<input type="hidden" name="uid" value="<?php echo $uid ?>">
			<input type="hidden" name="givenname" value="<?php echo $givenname ?>">
			<input type="hidden" name="surname" value="<?php echo $surname ?>">			
		<table class="table table-striped" id="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th><th scope="col">Name</th><th scope="col">Description</th>
				</tr>
			</thead>
			<tbody>
		<?php
		while ($course = mysqli_fetch_assoc($courses)) {
			$id = $course['id'];
    		$checked = in_array($id, $sel_courses) ? 'checked' : '';
    		echo "<tr>";
			echo "<th scope='row'><input type='checkbox' name='course[]' value='{$id}' {$checked}/></th>";
			echo "<td>{$course['title']}</td>";
			echo "<td>{$course['description']}</td>";
    		echo "</tr>\n";
    	}
		?>
			</tbody>
		</table>
		<button type="submit" class="btn btn-primary" id="submit" disabled>OK</button>
		</form>
		</div>
	</body>
</html>
<?php
mysqli_close($con);
?>