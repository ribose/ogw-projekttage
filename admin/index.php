<?php
define('TITLE', 'Projekttage ' . date("Y") . ' - Administration');

$program = 'linear_sum_assignment.py';
?>
<!doctype html>
<html lang="en">
	<head>
		<title><?php echo TITLE; ?></title>
		 <!-- Required meta tags -->
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="../bootstrap.min.css" type="text/css" />
	</head>
	<body>
		<script src="../jquery-3.3.1.min.js"></script>
		<script src="../bootstrap.min.js"></script>
		<script type="text/javascript">
			// When your page is ready, wire up these events
    		$(document).ready(function () {
    			$('a.program').on('click', function(e) {
    				e.preventDefault();
					var program = $(e.target).text();
  					$('#output').hide();
  					$('#output').html('');
					execute(program);
    			});
    		});

    		function execute(program) {
    			$.getJSON("exec.php?program=" + program, function(json) {
    				console.log("Got following data '" + JSON.stringify(json) + "' from server.");
    				var html = '<dl>'
    				$.each(json, function(index, item) {
              var output = item.output;
              if (output.startsWith("file://")) {
                var file = output.substring("file://".length);
                var href = program + "/" + file;
                html += "<dt>" + item.title + "</dt><dd><a href='"  + href + "'>" + file + "</a></dd>";
              } else {
                html += "<dt>" + item.title + "</dt><dd>" + output + "</dd>";
              }
			      });
					  html += '</dl>';
  					$('#output').html(html);
  					$('#output').show();
  				}).fail(function(err) {
  					var json = JSON.stringify(err);
    				console.log("Got following error '" + json + "' from server.");
    				var text = '<dl><dt>Error</dt><dd>' + json + '</dd></dl>';
  					$('#output').html(text);
  					$('#output').show();
  				})
    		}

		</script>

		<div class="container">
		<h2><?php echo TITLE; ?></h2>
		<h3>Programme</h3>
		<ul>
      <?php
      foreach(glob('*', GLOB_ONLYDIR) as $dir) {
        echo '<li><a href="#" class="program">' . $dir . '</a></li>';
      }
      ?>
    	</ul>
		<div class="alert alert-primary collapse" role="alert" id="output"></div>
		</div>
	</body>
</html>
