<?php
// Constants
define('DS', DIRECTORY_SEPARATOR);

$main = 'main.py';
$program = $_GET['program'];
$path = __DIR__ . DS . $program . DS . $main;
// headers for not caching the results
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
// headers to tell that result is JSON
header('Content-type: application/json');
$result["path"] = $path;
if (!is_executable($path)) {
	$result["title"] = "Error";
	$result["output"] = "Given path '{$path}' does NOT exist or is NOT executable.";
} else {
	$command = escapeshellcmd($path);
	chdir($program);
    # Use '2>&1' for debugging purposes: $output = shell_exec($command . " 2>&1");
    if (file_exists('bin/python')) {
    	# Use 'virtualenv' or ...
   		$output = shell_exec("bin/python " . realpath($command));
    } else {
    	# ... trust default installation
   		$output = shell_exec(realpath($command));
    }
 	$result["title"] = "Output";
	$result["output"] = $output;
}
echo json_encode(array($result));
?>