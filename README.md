# OGW Projekttage #

See http://docs.python-guide.org/en/latest/dev/virtualenvs/ for dependencies installation.

Perform following steps to be able to **PHP** run `project-day-distributor` from the _admin_ directory:

* `git clone https://github.com/ribeaud/project-day-distributor.git` to clone the project
* `chmod a+w .` to make the installation directory writable (needed by running application)
* `virtualenv -p /usr/local/bin/python2.7 .` to create an _virtual_ environment in the project
* `pip install -r requirements.txt` to install the requirements
* Write a file named `main.py` inside `project-day-distributor` folder with following content:
```
#!/usr/bin/env python2.7

import linear_sum_assignment
linear_sum_assignment.main(['linear_sum_assignment.py', 'PROD'])

print('file://assignment.xlsx')
```

## Tasks

* Add request headers
* How about a **Docker** image to make the deployment environment agnostic?