-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 03:19 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ogw`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` tinyint(4) UNSIGNED NOT NULL COMMENT 'Primary key',
  `title` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Course title (must be unique over the table)',
  `description` text COLLATE utf8_bin NOT NULL COMMENT 'Course description',
  `max_students` tinyint(4) NOT NULL COMMENT 'The maximum number of students this course could accept',
  `level_from` tinyint(3) UNSIGNED NOT NULL COMMENT 'Minimum required school level (inclusive)',
  `level_to` tinyint(3) UNSIGNED NOT NULL COMMENT 'Maximum required school level (inclusive)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `description`, `max_students`, `level_from`, `level_to`) VALUES
(1, 'Course 0', 'This is course 0.', 4, 5, 7),
(2, 'Course 1', 'This is course 1.', 2, 7, 12),
(3, 'Course 2', 'This is course 2.', 1, 5, 5),
(4, 'Course 3', 'This is course 3.', 10, 4, 7),
(5, 'Course 4', 'This is course 4.', 5, 6, 11),
(6, 'Course 5', 'This is course 5.', 2, 4, 6),
(7, 'Course 6', 'This is course 6.', 3, 8, 9),
(8, 'Course 7', 'This is course 7.', 1, 5, 12),
(9, 'Course 8', 'This is course 8.', 2, 5, 12),
(10, 'Course 9', 'This is course 9.', 1, 7, 12);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` smallint(5) UNSIGNED NOT NULL COMMENT 'Primary key',
  `uid` varchar(20) COLLATE utf8_bin NOT NULL COMMENT 'LDAP UID',
  `givenname` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Firstname',
  `surname` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Lastname',
  `prioritized_list` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Comma-separated list of chosen courses',
  `level` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'Current school level (examples are ''schule-5b'' or ''schule-10a'')'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `uid`, `givenname`, `surname`, `prioritized_list`, `level`) VALUES
(1, 'p.rib', 'Philomène', 'Ribeaud', '1,4,8', 'schule-10a'),
(2, 'l.gas', 'Laurent', 'Gasser', '1,6,7', 'schule-5b'),
(3, 'ph.rib', 'Phénicia', 'Ribeaud', '1,9,1', 'schule-5b'),
(4, 't.ber', 'Tanja', 'Berg', '2,4,1', 'schule-10a'),
(5, 'c.rib', 'Christian', 'Ribeaud', '2,8,7', 'schule-9a'),
(6, 'm.odi', 'Marcel', 'Odiet', '5,1,8', 'schule-4a'),
(7, 'n.odi', 'Nicolas', 'Odiet', '6,2,3', 'schule-8d'),
(8, 'ml.bol', 'Marie-Laure', 'Bollmann', '7,6,9', 'schule-5c'),
(9, 'k.ber', 'Katja', 'Berg', '8,2,7', 'schule-9a'),
(10, 'b.fis', 'Björn', 'Fisher', '1,9,2', 'schule-5a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key', AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
